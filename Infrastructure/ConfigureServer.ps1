[CmdletBinding()]
param (

    [Parameter(Mandatory = $true)]
    [string] $OctopusServerThumbprint,

    [Parameter(Mandatory = $true)]
    [string] $SQLAdminUser,

    [Parameter(Mandatory = $true)]
    [string] $SQLAdminPassword,

    [Parameter(Mandatory = $false)]
    [string] $OctopusServer,

    [Parameter(Mandatory = $false)]
    [string] $APIKey,

    [Parameter(Mandatory = $false)]
    [string] $TentaclePort,

    [Parameter(Mandatory = $false)]
    [String[]] $Roles,

    [Parameter(Mandatory = $false)]
    [string] $Environment,

    [Parameter(Mandatory = $false)]
    [string] $SpaceName
    
)

$tempDir = "C:\temp"
$chocolateyCacheLocation = "C:\temp\chocoCache"

mkdir $tempDir -Force
mkdir $chocolateyCacheLocation -Force

Start-Transcript -Path "C:\temp\serverConfigLog.txt" -Append

Write-Host "Installing IIS..."
Install-WindowsFeature -Name Web-WebServer -IncludeAllSubFeature -IncludeManagementTools

Import-Module WebAdministration
Write-Host "Removing Default Web Site..."
Get-WebSite -Name "Default Web Site" | Remove-WebSite -Confirm:$false -Verbose
& "netsh" advfirewall firewall add rule "name=Web Traffic" dir=in action=allow protocol=TCP localport=80

Write-Host "Installing Chocolatey..."
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#refresh environment variables
refreshenv

#SQL Server install fails without setting cacheLocation as it tries to extract in a system directory and it's not allowed
Write-Host "Setting choco config values"
choco config set --name cacheLocation --value c:\temp\choco | Write-Host

Write-Host "Installing software packages..."
choco install sql-server-express dotnet-5.0-runtime dotnet-5.0-windowshosting dotnetfx ssms octopusdeploy.tentacle -y
Write-Host "Software installed."


#set Temp dir otherwise tentacle config fails
$env:TEMP = $tempDir
$env:TMP = $tempDir

#Set the Variables for configuring Octopus Tentacle
$PublicIP = (Invoke-WebRequest http://myexternalip.com/raw -UseBasicParsing).content
Write-Host "Public IP: $PublicIP" 
$SpaceName = [uri]::UnescapeDataString($SpaceName)
$tentacleExe = "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe"
$serverParam = "--server=$OctopusServer"
$apiKeyParam = "--apiKey=$APIKey"
$publicHostNameParam = "--publicHostName=$PublicIP"
$environmentParam = "--environment=$Environment"
$spaceNameParam = "--space=$SpaceName"

$registerWithArgs = 'register-with', '--instance="Tentacle"', $serverParam, $apiKeyParam, $publicHostNameParam, $environmentParam, $spaceNameParam, '--comms-style=TentaclePassive', '--force', '--console'
foreach ($role in $Roles) {
    $registerWithArgs += "--role=$role"
}


Write-Host "Configuring Octopus Tentacle..."
& "netsh" advfirewall firewall add rule "name=Octopus Deploy Tentacle" dir=in action=allow protocol=TCP localport=$TentaclePort

Write-Host "  - Creating Instance"
& $tentacleExe create-instance --instance "Tentacle" --config "C:\Octopus\Tentacle.config" --console

Write-Host "  - Creating Certificate"
& $tentacleExe new-certificate --instance "Tentacle" --if-blank --console

Write-Host "  - Resetting trust"
& $tentacleExe configure --instance "Tentacle" --reset-trust --console

Write-Host "  - Configuring Tentacle properties"
& $tentacleExe configure --instance "Tentacle" --home "C:\Octopus" --app "C:\Octopus\Applications" --port "$TentaclePort" --noListen "False" --console

Write-Host "  - Configuring Tentacle to trust Octopus Server"
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" configure --instance "Tentacle" --trust "$OctopusServerThumbprint" --console

#& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" register-with --instance "Tentacle" --server "$OctopusServer" --apiKey="$APIKey" --publicHostName="$PublicIP" --role "$Role" --environment "$Environment" --space "$SpaceName" --comms-style TentaclePassive --force --console
Write-Host "  - Registering Tentacle"
& $tentacleExe $registerWithArgs

Write-Host "  - Installing and restarting Tentacle Windows Service"
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" service --instance "Tentacle" --install --stop --start --console
Write-Host "Octopus Tentacle Configured!"
Write-Host ""

Write-Host "Configuring SQL Server Express..."
$sqlServerConnectionString = "Server=(local)\SQLEXPRESS;Database=master;Integrated Security=True"
        
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection
$sqlConnection.ConnectionString = $sqlServerConnectionString

$command = $sqlConnection.CreateCommand()
$command.CommandType = [System.Data.CommandType]'Text'

$sqlConnection.Open()

Write-Host "  - Creating Admin login..."
$command.CommandText = "IF NOT EXISTS(SELECT 1 FROM sys.server_principals WHERE name = '$SQLAdminUser')
CREATE LOGIN [$SQLAdminUser] with Password='$SQLAdminPassword', default_database=master" 
$command.ExecuteNonQuery()

Write-Host "  - Adding admin user to role..."
$command.CommandText = "ALTER SERVER ROLE [sysadmin] ADD MEMBER [$SQLAdminUser]"
$command.ExecuteNonQuery()

$sqlConnection.Close()

Write-Host "  - Configuring SQL Server service..."
$sql2016RegistryPath = "HKLM:\Software\Microsoft\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQLServer"                
$sql2017RegistryPath = "HKLM:\Software\Microsoft\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQLServer"
$sql2019RegistryPath = "HKLM:\Software\Microsoft\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQLServer"
$sqlRegistryPathToUse = $null
if (Test-Path $sql2016RegistryPath)
{        
    $sqlRegistryPathToUse = $sql2016RegistryPath
}
elseif (Test-Path $sql2017RegistryPath)
{        
    $sqlRegistryPathToUse = $sql2017RegistryPath
}
elseif (Test-Path $sql2019RegistryPath)
{        
    $sqlRegistryPathToUse = $sql2019RegistryPath
}        

New-ItemProperty -Path $sqlRegistryPathToUse -Name "LoginMode" -Value "2" -PropertyType DWORD -Force
New-ItemProperty -Path "$sqlRegistryPathToUse\SuperSocketNetLib\Tcp" -Name "Enabled" -Value "1" -PropertyType DWORD -Force
New-ItemProperty -Path "$sqlRegistryPathToUse\SuperSocketNetLib\Np" -Name "Enabled" -Value "1" -PropertyType DWORD -Force
Set-ItemProperty -Path "$sqlRegistryPathToUse\SuperSocketNetLib\Tcp\IPAll" -Name "TcpPort" -Value "1433" -Force

net stop MSSQL`$SQLEXPRESS /y
net start MSSQL`$SQLEXPRESS


Write-Host ""
Write-Host ""
Write-Host "Script complete. Restarting computer..."
Write-Host ""
Write-Host ""
Write-Host ""

Stop-Transcript

Restart-Computer